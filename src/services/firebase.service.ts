// import { HttpClient } from '@angular/common/http';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import 'rxjs/add/operator/map'

@Injectable()
export class FirebaseService {

  constructor(
    public http: Http,
    ) {
        
  }

  getArchives() {
    return this.http
      .get(environment.databaseURL)
      .map(res => res.json());
  }

  getFiles() {
    return this.http
      .get(environment.databaseURL + '/archives/') // + archiveId
      .map(res => res.json());
  }

  addArchive(archiveId, item) {
    let params = JSON.stringify(item);
    return this.http
      .post(environment.databaseURL + '/archives/', params)
      .map(res => res.json());
  }

  addItem(archiveId, item) {
    let params = JSON.stringify(item);
    return this.http
      .post(environment.databaseURL + '/archives/archives/' + archiveId, params)
      .map(res => res.json());
  }

  editItem(archiveId, fileId, item) {
    let params = JSON.stringify(item);
    return this.http
      .put(environment.databaseURL + '/archives/archives/' + 
          archiveId + '/files/' + fileId, params)
      .map(res => res.json());
  }

  removeItem(archiveId, fileId, item) {
    let params = JSON.stringify(item);
    return this.http
      .delete(environment.databaseURL + '/archives/' +
        archiveId + '/' + fileId, params)
      .map(res => res.json());
  }

}
