import { Injectable } from "@angular/core";
import { Archive } from '../models/archive.model';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable()
export class ArchiveService {
    archiveRef = this.db.list<Archive>('/archives');

    constructor(
        private db: AngularFireDatabase
    ) {
        
    }

    /*
    @Effect()
    getData$ = this.dataDb.getFakeDataStateChanges().pipe(
        mergeMap(actions => actions),
        map(action => {
            return {
                type: `[FirstData] ${action.type}`,
                payload: { 
                    id: action.payload.doc.id, 
                    ...action.payload.doc.data() 
                }
            };
        })
    );

    @Effect()
    loadPosts$ = this.actions$.pipe(
        ofType(LOAD_POSTS),
        pluck(‘payload’),
        switchMap(uid =>
                this.db.getAllPosts(uid).pipe(
                    takeUntil(this.subService.unsubscribe$)
                ),
            map(posts => new LoadPostsSuccess({ entries: posts }))
            )
        )
    );
    */

    getArchives() {
        return this.archiveRef;
    }

    addArchive(archive: Archive) {
        return this.archiveRef.push(archive);
    }

    editArchive(archive: Archive) {
        return this.archiveRef.update(archive.key, archive);
    }

    removeArchive(archive: Archive) {
        return this.archiveRef.remove(archive.key);
    }

}