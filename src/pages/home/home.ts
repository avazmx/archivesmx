import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { FilePage } from '../file/file';
// import { AngularFireDatabase } from 'angularfire2/database';
// import { File } from '../../models/file.model';
import { Archive } from '../../models/archive.model';
import { ArchiveService } from '../../services/archive.service';
import { Observable } from 'rxjs/Observable';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  show: boolean;
  files: any;
  item: any;
  archiveName: any;
  archive: Archive = {
    // key: undefined,
    name: '',
    id: undefined,
  };
  name: any;
  archives$: Observable<Archive[]>;
  showEdit: boolean;

  constructor(
    public navCtrl: NavController,
    private _archiveService: ArchiveService,
    ) {
    this.showEdit = false;      
  }

  delay(milliseconds: number): Promise<number> {
    return new Promise<number>(resolve => {
      setTimeout(() => {
        resolve();
      }, milliseconds);
    });
  }

  goToFile(archiveId, fileId) {
    // this.delay(100).then(any => {
      this.navCtrl.push(FilePage, {
        archiveId: archiveId,
        fileId: fileId,
        archives: this.archives$,
      })
    // })
  }

  displayContent(i) {
    switch(i) {
      case 0: return 'fast400';
      case 1: return 'fast500';
      case 2: return 'fast600';
      case 3: return 'fast700';
      case 4: return 'fast800';
      case 5: return 'fast900';
      default: '';
    }
  }

}
