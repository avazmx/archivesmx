import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';
import { ArchivesComponent } from '../../components/archives/archives';

@NgModule({
    declarations: [ 
        HomePage,
        ArchivesComponent,
    ],
    imports: [ 
        IonicPageModule.forChild(HomePage),
    ]
})

export class FilePageModule { }
