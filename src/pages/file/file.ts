import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FirebaseService } from '../../services/firebase.service';

@IonicPage()
@Component({
  selector: 'page-file',
  templateUrl: 'file.html',
})
export class FilePage {
  fileId: any;
  archiveId: any;
  archives: any;
  fileInfo: any;
  file: any;
  element: any;
  edit: boolean;
  item: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private _firebase: FirebaseService,
  ) {
    this.archiveId = navParams.get('archiveId');
    this.fileId = navParams.get('fileId');
    this.archives = navParams.get('archives');
    console.log(this.archives);
    console.log(this.fileId);
    console.log(this.archiveId);
    this.element = this.archives[this.archiveId - 1].files[this.fileId - 1];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FilePage');
  }

  editMode() {
    this.edit = !this.edit;
  }

  done() {
    this.navCtrl.pop();
    this.edit = false;
    console.log(this.fileId - 1);
    console.log(this.archiveId - 1);
    console.log(this.element);
    
    this._firebase.editItem(this.archiveId - 1, this.fileId - 1, this.element)
      .subscribe( res => {
        this.element = res;
        console.log(this.element);
      });
  }

}
