import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { FilePage } from '../pages/file/file';
import { ArchiveService } from '../services/archive.service';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FirebaseService } from '../services/firebase.service';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule, AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { StoreModule } from '@ngrx/store';
import { ArchivesReducer } from '../components/archives/store/archives.reducers';
import { EffectsModule } from '@ngrx/effects';
import { ArchivesEffects } from '../components/archives/store/archives.effects';

@NgModule({
  declarations: [
    MyApp,
    FilePage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    IonicModule.forRoot(MyApp),
    StoreModule.forRoot({archives : ArchivesReducer}),
    EffectsModule.forRoot([ArchivesEffects]),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    FilePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AngularFireDatabase,
    FirebaseService,
    ArchiveService,
  ]
})
export class AppModule {}
