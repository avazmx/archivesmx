export interface Archive {
    key?: string;
    name: string;
    id: number;
}