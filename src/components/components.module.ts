import { NgModule } from '@angular/core';
import { ArchivesComponent } from './archives/archives';

@NgModule({
	declarations: [ArchivesComponent],
	imports: [],
	exports: [ArchivesComponent]
})

export class ComponentsModule {}
