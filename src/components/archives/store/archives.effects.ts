import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { AngularFireDatabase } from '@angular/fire/database';

import { Observable, of } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';

import * as ArchivesActions from './archives.actions';

// import { Archive } from '../../../models/archive.model';
// import { ArchiveService } from '../../../services/archive.service';
// import { HttpClient } from '@angular/common/http'

export type Action = ArchivesActions.ArchivesActions;

@Injectable()
export class ArchivesEffects {
    // archiveActions: ArchivesActions.ArchivesActions;
    // archiveRef = this.db.list<Archive>('/archives');
 
    constructor(
            private actions: Actions,
            private db: AngularFireDatabase,
            // private http: HttpClient,
            // private _archiveService: ArchiveService,
        ) {

    }

    @Effect()
    addArchive: Observable<Action> = this.actions.ofType(ArchivesActions.ADD_ARCHIVE)
        .pipe(
            map((action: ArchivesActions.AddArchive) => action.payload),
            mergeMap(payload => of(this.db.object('archives/')
                .update({
                    archives: payload.archives + payload.val
                }))),
            map(() => new ArchivesActions.AddArchiveSuccess()),
            catchError(err => of(new ArchivesActions.AddArchiveFail({ error: err.message}) )));


            /*
        .map(archive => {
            archive.key = archive.key;
            return new ArchivesActions.AddArchiveSuccess(archive);
        })
        */
    
    /*
    @Effect()
    FetchEvents$: Observable<Archive> = this.actions$
        .ofType(this.archiveActions.toString()) // filtering actions
        .switchMap(action => this._archiveService.archiveRef
        .do((payload) => this.archiveActions(payload)) 
        // map or do, this.ArchivesActions.FetchEventsSuccess
        );

    @Effect()
    loadAuths$: Observable<Action> = this.actions$.pipe(
        ofType(authActions.AuthActionTypes.LoadAuths),
        switchMap(() => {
        return this.http.get<string>('login')
            .pipe(
            map((userName) => {
                return new authActions.SetAuths(userName);
            })
            )
        })
    );
    */

}