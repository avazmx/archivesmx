import { Action } from '@ngrx/store';
import { Archive } from '../../../models/archive.model';

export const ADD_ARCHIVE = 'Add archive';
export const ADD_ARCHIVE_SUCCESS = 'Add archive success';
export const ADD_ARCHIVE_FAIL = 'Add archive failed';

export class AddArchive implements Action {
    readonly type = ADD_ARCHIVE;
    // payload: Archive;
    constructor(public payload: any) {}
}

export class AddArchiveSuccess implements Action {
    readonly type = ADD_ARCHIVE_SUCCESS;
    constructor(public payload?: any) {}
}

export class AddArchiveFail implements Action {
    readonly type = ADD_ARCHIVE_FAIL;
    constructor(public payload?: any) {}
}

export type ArchivesActions 
    = AddArchive 
    | AddArchiveSuccess
    | AddArchiveFail;