import * as ArchivesActions from './archives.actions';

import { Archive } from '../../../models/archive.model';

/* 
const initialState = {
    archives: [ { name: 'hola' }, ]
}
*/

export type Action = ArchivesActions.ArchivesActions;

export function ArchivesReducer(
    // state = initialState, 
    state: Archive, action: Action
    ) {
    switch(action.type) {

        case ArchivesActions.ADD_ARCHIVE:
            return { ...state, loading: true };

        case ArchivesActions.ADD_ARCHIVE_SUCCESS:
            return { ...state, ...action.payload, loading: false };
            
        case ArchivesActions.ADD_ARCHIVE_FAIL:
            return { ...state, ...action.payload, loading: false };

        default:
            return state;
    }
}
