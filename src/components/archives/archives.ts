import { Component } from '@angular/core';
import { Archive } from '../../models/archive.model';
import { Observable } from 'rxjs/Observable';
import { ArchiveService } from '../../services/archive.service';
import { ItemSliding } from 'ionic-angular';
import { Store } from '@ngrx/store';

@Component({
  selector: 'archives',
  templateUrl: 'archives.html'
})
export class ArchivesComponent {
  archive: Archive = {
    // key: undefined,
    name: '',
    id: undefined,
  };
  // archives$: Observable<{archives: Archive[]}>;
  archivesState: Observable<{archives: Archive[]}>;
  showEdit: boolean;

  constructor(
    // public navCtrl: NavController,
    private _archiveService: ArchiveService,
    private store: Store<{archives: {archives: Archive[]}}>, // could be any --> Store<{}>
    ) {
    this.showEdit = false;
    /*
    this.archives$ = this._archiveService
      .getArchives()
      .snapshotChanges() // Key and Value
      .map( changes => {
        this.archive.id = changes.length;
        console.log(changes);
          return changes.map( c => ({
            key: c.payload.key, 
            ...c.payload.val(),
          }));
      });
    */
    this.archivesState = this.store.select('archives');
    console.log(this.archivesState);
  }
  
  addArchive(archive: Archive) {
    console.log(this.archive.name);
    this._archiveService.addArchive(archive).then(ref => {
      this.archive.key = ref.key;
      console.log(archive);
    });
  }

  deleteArchive(archive: Archive) {
    this._archiveService.removeArchive(archive);
  }

  editMode(i, item: ItemSliding) {
    this.showEdit = true;
    item.close();
    console.log(i);    
  }

  submitEdit(archive: Archive) {
    this._archiveService.editArchive(archive);
    this.showEdit = false;
  }

}
